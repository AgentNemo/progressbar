# progressbar

A simple single line progress bar with modulation in mind. **Concurrency save!**

## Features

- [x] Modularization
- [x] io.Writer implementation
- [x] Colors support

## Usage

### Basic

The progressbar only contains the percentage element.
````go
pb := spb.New(100)
for i := 0; i < 100; i++ {
    pb.Advance(1)
    // do something
    time.Sleep(time.Millisecond * 500)
}
````

Create a progressbar without limit
````go
pb := spb.NewNoLimit()
for i := 0; i < 1000; i++ {
    pb.Advance(1)
    // do something
    time.Sleep(time.Millisecond * 500)
}
````

Set or add elements
````go
pb := spb.New(100)
pb.Render.SetElements(spb.NewProgressPerTime(*spb.NewBase("<", ">")))
pb.Render.AddElements(spb.NewProgressPerTime(*spb.NewBase("<", ">")))
for i := 0; i < 100; i++ {
    pb.Advance(1)
    // do something
    time.Sleep(time.Millisecond * 500)
}
````

All methods for manipulating the progress
````go
pb := spb.New(100)
for i := 0; i < 100; i++ {
    pb.Advance(1)
    pb.Increment()
    pb.Decrement()
    pb.Set(500)
    pb.Reduce(30)
    // do something
    time.Sleep(time.Millisecond * 500)
}
````

You can also use method chaining. I don't know why you should use it but you can.
````go
pb := spb.New(100)
for i := 0; i < 100; i++ {
    pb.Advance(1).Increment().Decrement().Set(500).Reduce(30)
    // do something
    time.Sleep(time.Millisecond * 500)
}
````

### Download

You can also download files with a progressbar. You can use every io.Writer you want to copy the files to.
````go
out, err := os.Create("abc.zip")
if err != nil  {
    log.Fatal(err)
}
defer out.Close()
spb.NewDownload("http://ipv4.download.thinkbroadband.com/200MB.zip", out)
````

![download](./assets/download.gif)

### Reader/Writer

Use a Reader and Writer.

````go
out, err := os.Create("abc2.zip")
if err != nil  {
    log.Fatal(err)
}
defer out.Close()
in, err := os.Open("abc.zip")
if err != nil  {
    log.Fatal(err)
}
defer in.Close()
spb.NewCopy(in, out)
````

### Self run

Blocking self run until finished
````go
pb := spb.New(100)
pb.SelfRun(1000, 10)
````

### File

Creates a progressbar depending on the file size.

````go
	pb, size, err := spb.NewByFile("./README.md")
	if err != nil {
		log.Fatal(err)
	}
	for i := 0; i < int(size); i++ {
		pb.Advance(1)
		time.Sleep(time.Millisecond * 100)
	}
````

### Pipe

Creates a progressbar with the max count piped at commandline
The progressbar only contains the percentage element.

````go
pb, count, err  := spb.NewByPipe()
if err != nil {
    log.Fatal(err)
}
for i := 0; i < int(count); i++ {
    pb.Advance(1)
    time.Sleep(time.Millisecond * 100)
}
````

Example
````shell script
echo 50 | go run main.go
````

### Channel

Creates a progressbar which gets a channel for communicate the progress

NOTE: The call is blocked until the bar is finished

````go
ch := make(chan uint, 0)
spb.NewByCall(ch, func(uints chan uint) {
    // creates the progressbar
    ch <- 50
    for {
        time.Sleep(time.Second)
        // advances the progressbar
        ch <- 1
    }
})
````

### Colors 

Each element can be colorized.

````go
pb.Render.SetElements(NewTimeCount(FirstBase()).AddColor(Green),
    NewTimeRemaining(MiddleBase()).AddColor(Reset),
    NewProgressPerTime(MiddleBase()).AddColor(Red),
    NewCycle(5, NewTimeRemaining(LastBase()),
        NewAbsoluteProgress(LastBase(), " / "),
        NewStatic(LastBase(), "Showcase3").AddColor(Blue),
        NewBar(LastBase(), '+', '-', '+', true).SetSize(30).AddColor(Red)))
````

### Elements

As mention you can build the progressbar as you like. You can take a look at Options.go to see prebuiled designs.
There are also showcase options you can use to see every element in action.

**Note: After each element rendering the current length is compared to the size provided by the output.
If the current elements length reaches the size the subsequent elements are not rendered.**

#### Base

This element is the simplest one. It contains a start string and an end string.
Normally each other element receives at his first parameter this element.

| Property        | Type           | Description  |
| ------------- |:-------------:| -----:|
| prefix     | string | starting string |
| postfix      | string      |   ending string |

#### Absolute

Shows the current position.

| Property        | Type           | Description  |
| ------------- |:-------------:| -----:|
| base     | Base | Base element |

#### AbsoluteProgress

Shows the current position and the max of the progressbar.
** NOTE: Does not work correctly with NewNoLimit. Use instead Absolute**

| Property        | Type           | Description  |
| ------------- |:-------------:| -----:|
| base     | Base | Base element |
| separator      | string      | string which separates the two values |

#### AbsoluteProgressSize

Works like AbsoluteProgress with the ability to show a size unit.
If the unit is empty the unit shown is calculated every rendering.
** NOTE: Does not work correctly with NewNoLimit. Use instead AbsoluteSize**

| Property        | Type           | Description  |
| ------------- |:-------------:| -----:|
| absolute     | Absolute | Absolute element |
| unit      | string      | the unit to be shown |

#### AbsoluteSize

Works like Absolute with the ability to show a size unit.
If the unit is empty the unit shown is calculated every rendering.

| Property        | Type           | Description  |
| ------------- |:-------------:| -----:|
| absolute     | Absolute | Absolute element |
| unit      | string      | the unit to be shown |

#### Bar

The bar in progressbar. This element is rendered at the end.
The bar size is rendered dynamicly depending on the size left on the progressbar. 
If two bars are configured they split the remaining space into two. 

| Property        | Type           | Description  |
| ------------- |:-------------:| -----:|
| base     | Base | Base element |
| fill      | rune      | the rune to use for the fill part |
| empty      | rune      | the rune to use for the empty part |
| tip      | rune      | the rune to use for the tip of the fill part |
| static      | bool      | defines if the bar size is static |


#### Cycle

Cycles shows every tick another element.

| Property        | Type           | Description  |
| ------------- |:-------------:| -----:|
| elements     | []Element | List of element |

#### Percentage

Shows the percentage 

| Property        | Type           | Description  |
| ------------- |:-------------:| -----:|
| base     | Base | Base element |

#### ProgressPerTime

Shows the absolute progress over time 

| Property        | Type           | Description  |
| ------------- |:-------------:| -----:|
| base     | Base | Base element |

#### Static

A Static value.

| Property        | Type           | Description  |
| ------------- |:-------------:| -----:|
| base     | Base | Base element |
| value     | string | string |

#### Time

The basic time element.  

| Property        | Type           | Description  |
| ------------- |:-------------:| -----:|
| time     | Time | Time  |
| secStr     | string | string to show after the seconds value  |
| minStr     | string | string to show after the minutes value  |
| hourStr     | string | string to show after the hours value  |

#### TimeCount

Counts the time depending on the start time. 
Shows the time in the format "5h3m6s"


| Property        | Type           | Description  |
| ------------- |:-------------:| -----:|
| base     | Base | Base element |

#### TimeRemaining

Shows the time remaining until the progress if finish.
The value is calculated on every rendering to provide accuracy. 

| Property        | Type           | Description  |
| ------------- |:-------------:| -----:|
| base     | Base | Base element |

##### Showcases

Create a new progressbar with the showcase options

````go
pb := spb.New(100, spb.Showcase1)
for i := 0; i < 100; i++ {
    pb.Advance(1)
    time.Sleep(time.Millisecond * 100)
}
````

### Outputs

You can define your own outputs if you like. The default one is stdin.

#### File

You can also define a file as an output. 

**Note: There is no check for the colors to not been rendered if the output is a file**

````go
pb := spb.New(100, spb.Showcase1, spb.OutputFile("text.log", 50, 0))
for i := 0; i < 100; i++ {
    pb.Advance(1)
    // do something
    time.Sleep(time.Millisecond * 10)
}
````
