package main

import (
	"gitlab.com/AgentNemo/progressbar/spb"
	"testing"
)

func Benchmark(b *testing.B) {
	pb := spb.New(100, spb.Showcase2)
	for i := 0; i < 100; i++ {
		pb.Advance(1)
	}
	pb.Finish()
}
