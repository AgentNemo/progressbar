package spb

// Element interface
type Element interface {
	Compile(pb *ProgressBar) string // compiles the outputs
	Color() Color                   // color to be displayed
	Dynamic() bool
	SetSize(int)
}
