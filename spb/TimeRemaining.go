package spb

import (
	"fmt"
	"math"
	"time"
)

// TimeRemaining
type TimeRemaining struct {
	Time
}

func NewTimeRemaining(base Base) *TimeRemaining {
	return &TimeRemaining{
		Time: *NewTimeDefault(base),
	}
}

func (t *TimeRemaining) Compile(pb_ *ProgressBar) string {
	dif := time.Now().Sub(t.Time.Value)
	sec := int(math.Round((dif.Seconds() / float64(pb_.Current)) * float64(pb_.Limit-pb_.Current)))
	return fmt.Sprintf("%s%s%s", t.Prefix, ConvSec(t.Time, sec), t.Postfix)
}

func (t *TimeRemaining) AddColor(color Color) *TimeRemaining {
	t.Base.SetColor(color)
	return t
}
