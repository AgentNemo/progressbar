package spb

import (
	"fmt"
	"time"
)

type ProgressPerTimeSize struct {
	Base
	Time      time.Time
	position  uint
	Unit      string
	lastValue float64
}

func NewProgressPerTimeSize(base Base, unit string) *ProgressPerTimeSize {
	return &ProgressPerTimeSize{
		Base:     base,
		Time:     time.Now(),
		position: 1,
		Unit:     unit,
	}
}

func (p *ProgressPerTimeSize) Compile(pb *ProgressBar) string {
	positionPerSecond := p.lastValue
	if time.Now().Sub(p.Time) > time.Second {
		p.Time = time.Now()

		positionPerSecond := float64(pb.Current - p.position)
		positionPerSecond = (positionPerSecond + p.lastValue) / 2.0

		p.position = pb.Current
		p.lastValue = positionPerSecond
	}

	position := ""
	if p.Unit == "" {
		unit := ConvUnitSize(positionPerSecond)
		position = ConvFileSize(unit, positionPerSecond)
	} else {
		position = ConvFileSize(p.Unit, positionPerSecond)
	}

	return fmt.Sprintf("%s%s/s%s", p.Prefix, position, p.Postfix)
}

func (p *ProgressPerTimeSize) AddColor(color Color) *ProgressPerTimeSize {
	p.Base.SetColor(color)
	return p
}
