package spb

import (
	"fmt"
	"time"
)

// Time counting
type TimeCount struct {
	Time
}

func NewTimeCount(base Base) *TimeCount {
	return &TimeCount{
		Time: *NewTimeDefault(base),
	}
}

func (t *TimeCount) Compile(pb *ProgressBar) string {
	dif := time.Now().Sub(t.Time.Value)
	return fmt.Sprintf("%s%s%s", t.Prefix, ConvSec(t.Time, int(dif.Seconds())), t.Postfix)
}

func (t *TimeCount) AddColor(color Color) *TimeCount {
	t.Base.SetColor(color)
	return t
}
