package spb

import "time"

type Options func(pb *ProgressBar)

func OutputCLI(pb *ProgressBar) {
	pb.SetOutput(*NewOutputCLI())
}

func OutputFile(path string, width uint, offset int64) func(pb *ProgressBar) {
	output := NewOutputFile(path, width, offset)
	return func(pb *ProgressBar) {
		pb.SetOutput(*output)
	}
}

func FirstBase() Base {
	return *NewBase("| ", " ")
}

func MiddleBase() Base {
	return *NewBase(" ", " ")
}

func LastBase() Base {
	return *NewBase(" ", " |")
}

func RenderDefault(pb *ProgressBar) {
	pb.Render.SetElements(NewPercentage(FirstBase()),
		NewBar(LastBase(), '#', '-', '#', false),
		NewProgressPerTime(*NewBase("<", ">")),
	)
}

func RenderNoLimit(pb *ProgressBar) {
	pb.Render.SetElements(NewTimeCount(FirstBase()), NewProgressPerTime(MiddleBase()), NewAbsolute(LastBase()))
}

func RenderFile(size uint, name string) func(pb *ProgressBar) {
	return func(pb *ProgressBar) {
		pb.Render.SetElements(NewStatic(FirstBase(), name),
			NewBar(MiddleBase(), '-', ' ', '>', false),
			NewPercentage(MiddleBase()),
			NewAbsoluteProgressSize(*NewAbsoluteProgress(MiddleBase(), " / "), ConvUnitSize(float64(size))),
			NewProgressPerTimeSize(MiddleBase(), ""),
			NewTimeRemaining(LastBase()))
	}
}

func RenderFileNoSize(pb *ProgressBar) {
	pb.Render.SetElements(NewTimeCount(FirstBase()),
		NewProgressPerTimeSize(MiddleBase(), ""),
		NewAbsoluteSize(*NewAbsolute(LastBase()), ""))
}

func Showcase1(pb *ProgressBar) {
	pb.Render.SetElements(NewPercentage(FirstBase()),
		NewBar(MiddleBase(), '-', ' ', '>', false),
		NewAbsoluteProgress(MiddleBase(), " / "),
		NewTimeRemaining(LastBase()))
}

func Showcase2(pb *ProgressBar) {
	pb.Render.SetElements(NewBar(FirstBase(), '#', '-', '#', false),
		NewBar(MiddleBase(), '+', '-', '+', true).SetBarSize(30),
		NewProgressPerTime(MiddleBase()),
		NewStatic(LastBase(), "Showcase2"))
}

func Showcase3(pb *ProgressBar) {
	pb.Render.SetElements(NewTimeCount(FirstBase()).AddColor(Green),
		NewTimeRemaining(MiddleBase()).AddColor(Reset),
		NewProgressPerTime(MiddleBase()).AddColor(Red),
		NewCycle(NewTimeRemaining(LastBase()),
			NewAbsoluteProgress(LastBase(), " / "),
			NewStatic(LastBase(), "Showcase3").AddColor(Blue),
			NewBar(LastBase(), '+', '-', '+', false).AddColor(Red),
		).ProcentualDuration(5))
}

func Showcase4(pb *ProgressBar) {
	pb.Render.SetElements(
		NewCycle(
			NewStatic(MiddleBase(), "EveryTick").AddColor(Blue),
			NewStatic(MiddleBase(), "EveryTick").AddColor(Red),
			NewStatic(MiddleBase(), "EveryTick").AddColor(Yellow),
		).EveryChange(true),
		NewCycle(
			NewStatic(MiddleBase(), "Every10%").AddColor(Blue),
			NewStatic(MiddleBase(), "Every10%").AddColor(Red),
			NewStatic(MiddleBase(), "Every10%").AddColor(Yellow),
		).ProcentualDuration(10),
		NewCycle(
			NewStatic(MiddleBase(), "EverySecond").AddColor(Blue),
			NewStatic(MiddleBase(), "EverySecond").AddColor(Red),
			NewStatic(MiddleBase(), "EverySecond").AddColor(Yellow),
		).TimeDuration(time.Second),
		NewCycle(
			NewStatic(MiddleBase(), "Every20ValuesAdded").AddColor(Blue),
			NewStatic(MiddleBase(), "Every20ValuesAdded").AddColor(Red),
			NewStatic(MiddleBase(), "Every20ValuesAdded").AddColor(Yellow),
		).AbsoluteDuration(20),
	)
}
