package spb

import (
	"github.com/olekukonko/ts"
	"io"
	"os"
)

type Output struct {
	Init  func()                // Do something on the start
	Clear func(thoroughly bool) // Clears the old bar
	Draw  func(str string)      // Draws the new bar
	Size  func() int            // Returns the available size
	Close func()                // Closes the Output
}

func NewOutputCLI() *Output {
	output := Output{
		Init: func() {
			os.Stdout.Write([]byte("\r"))
		},
		Clear: func(thoroughly bool) {

			if thoroughly {
				os.Stdout.Write([]byte("\r"))
				terminalSize, _ := ts.GetSize()
				size := terminalSize.Col()
				for i := 0; i < size; i++ {
					os.Stdout.Write([]byte(" "))
				}
			}
			os.Stdout.Write([]byte("\r"))
		},
		Draw: func(str string) {
			// TODO: cape to max size
			os.Stdout.Write([]byte(str))
		},
		Size: func() int {
			terminalSize, _ := ts.GetSize()
			return terminalSize.Col()
		},
		Close: func() {
			os.Stdout.Write([]byte("\n"))
		},
	}
	return &output
}

func NewOutputFile(path string, width uint, offset int64) *Output {
	f, _ := os.Create(path)
	output := Output{
		Init: func() {

		},
		Clear: func(thoroughly bool) {
			if thoroughly {
				f.Seek(offset, io.SeekStart)
				for i := 0; i < int(width); i++ {
					f.WriteString(" ")
				}
			}
			f.Seek(offset, io.SeekStart)
		},
		Draw: func(str string) {
			data := []byte(str)
			for i := 0; i < len(data) && i < int(width); i++ {
				f.Write([]byte{data[i]})
			}
		},
		Size: func() int {
			return int(width)
		},
		Close: func() {
			f.WriteString("\n")
			f.Close()
		},
	}
	return &output
}
