package spb

import (
	"fmt"
)

// Base struct
type Base struct {
	Prefix  string // Prefix
	Postfix string // Postfix
	color   Color
	dynamic bool
	size    int
}

func NewBase(prefix string, postfix string) *Base {
	return &Base{
		Prefix:  prefix,
		Postfix: postfix,
		color:   Reset,
		dynamic: false,
		size:    0,
	}
}

func (b *Base) Compile(pb *ProgressBar) string {
	return fmt.Sprintf("%s%s", b.Prefix, b.Postfix)
}

func (b *Base) SetColor(color Color) *Base {
	b.color = color
	return b
}

func (b *Base) Color() Color {
	return b.color
}

func (b *Base) Dynamic() bool {
	return b.dynamic
}

func (b *Base) SetSize(size int) {
	b.size = size
}
