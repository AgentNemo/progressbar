package spb

import (
	"fmt"
)

// Percentage value
type Percentage struct {
	Base
}

func NewPercentage(base Base) *Percentage {
	return &Percentage{base}
}

func (p *Percentage) Compile(pb *ProgressBar) string {
	percent := float64(pb.Current) / float64(pb.Limit) * 100.0
	content := fmt.Sprintf("%s%d%%%s", p.Prefix, uint(percent), p.Postfix)
	return content
}

func (p *Percentage) AddColor(color Color) *Percentage {
	p.Base.SetColor(color)
	return p
}
