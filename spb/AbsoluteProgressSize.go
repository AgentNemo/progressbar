package spb

import (
	"fmt"
)

// AbsoluteProgress file value
type AbsoluteProgressSize struct {
	AbsoluteProgress
	Unit string
}

func NewAbsoluteProgressSize(absolute AbsoluteProgress, unit string) *AbsoluteProgressSize {
	return &AbsoluteProgressSize{
		AbsoluteProgress: absolute,
		Unit:             unit,
	}
}

func (a *AbsoluteProgressSize) Compile(pb *ProgressBar) string {
	position := ""
	max := ""
	if a.Unit == "" {
		unit := ConvUnitSize(float64(pb.Limit))
		position = ConvFileSize(unit, float64(pb.Current))
		max = ConvFileSize(unit, float64(pb.Limit))

	} else {
		position = ConvFileSize(a.Unit, float64(pb.Current))
		max = ConvFileSize(a.Unit, float64(pb.Limit))
	}
	content := fmt.Sprintf("%s%s%s%s%s", a.Prefix, position, a.Separator, max, a.Postfix)
	return content
}

func (a *AbsoluteProgressSize) AddColor(color Color) *AbsoluteProgressSize {
	a.Base.SetColor(color)
	return a
}
