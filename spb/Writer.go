package spb

import (
	"io"
)

type Writer struct {
	Writer io.Writer
	*ProgressBar
}

func (d *Writer) Write(p []byte) (n int, err error) {
	n, err = d.Writer.Write(p)
	if err != nil {
		return 0, err
	}
	d.Advance(uint(n))
	return n, err
}
