package spb

import (
	"fmt"
	"strings"
)

type Render interface {
	GetSize() int
	Draw(pb *ProgressBar)
	Clear(thoroughly bool)
	AddElements(elements ...Element)
	SetElements(elements ...Element)
	Finish()
	SetOutput(output Output)
}

// SimpleRender struct
type SimpleRender struct {
	Elements     []Element // elements to renders
	Output       Output    // renders outputs
	RenderLength int       // previous length calculated render length
}

func (render *SimpleRender) GetSize() int {
	return render.Output.Size()
}

func (render *SimpleRender) Draw(pb *ProgressBar) {
	width := render.GetSize()
	outputs := make([]string, len(render.Elements))
	dynamics := make([]int, 0)
	output := ""
	length := 0

	// compile everything except bars
	for i, object := range render.Elements {
		// if length has reached width stop rendering
		if length >= width-1 {
			break
		}
		// skip if dynamic element
		if object.Dynamic() {
			dynamics = append(dynamics, i)
			continue
		}
		// compile element
		str := object.Compile(pb)
		length += len(str)
		outputs[i] = fmt.Sprintf("%s%s%s", object.Color(), str, Reset)
	}

	// build dynamic elements
	if len(dynamics) > 0 {
		newOutput, newLength := render.renderBars(pb, width, length, dynamics, outputs)
		outputs = newOutput
		length += newLength
	}

	// build string
	output = strings.Join(outputs, "")

	// check if string before was longer and fill with spaces if needed
	if render.RenderLength > length {
		for i := 0; i < render.RenderLength-length; i++ {
			output += " "
		}
	}

	render.RenderLength = length
	render.Clear(false)
	render.Output.Draw(output)
}

func (render *SimpleRender) renderBars(pb *ProgressBar, width int, length int, dynamics []int, output []string) ([]string, int) {
	// render bars
	barSize := (width - length) / len(dynamics)
	if barSize > 0 {
		for _, dynamic := range dynamics {
			object := render.Elements[dynamic]
			object.SetSize(barSize)
			str := object.Compile(pb)
			length += len(str)
			str = fmt.Sprintf("%s%s%s", object.Color(), str, Reset)
			output[dynamic] = str
		}
	}
	return output, length
}

func (render *SimpleRender) Clear(thoroughly bool) {
	render.Output.Clear(thoroughly)
}

func (render *SimpleRender) AddElements(elements ...Element) {
	render.Elements = append(render.Elements, elements...)
}

func (render *SimpleRender) SetElements(elements ...Element) {
	render.Elements = elements
}

func (render *SimpleRender) Finish() {
	render.Output.Close()
}

func (render *SimpleRender) SetOutput(output Output) {
	render.Output = output
}
