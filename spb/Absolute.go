package spb

import (
	"fmt"
)

// Absolute value
type Absolute struct {
	Base
}

func NewAbsolute(base Base) *Absolute {
	return &Absolute{
		Base: base,
	}
}

func (a *Absolute) Compile(pb *ProgressBar) string {
	content := fmt.Sprintf("%s%d%s", a.Prefix, pb.Current, a.Postfix)
	return content
}

func (a *Absolute) AddColor(color Color) *Absolute {
	a.Base.SetColor(color)
	return a
}
