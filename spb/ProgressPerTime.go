package spb

import (
	"fmt"
	"time"
)

type ProgressPerTime struct {
	Base
	Time      time.Time
	position  uint
	lastValue float64
}

func NewProgressPerTime(base Base) *ProgressPerTime {
	return &ProgressPerTime{
		Base:     base,
		Time:     time.Now(),
		position: 1,
	}
}

func (p *ProgressPerTime) Compile(pb *ProgressBar) string {
	positionPerSecond := p.lastValue
	if time.Now().Sub(p.Time) > time.Second {
		p.Time = time.Now()

		positionPerSecond := float64(pb.Current - p.position)
		positionPerSecond = (positionPerSecond + p.lastValue) / 2.0

		p.position = pb.Current
		p.lastValue = positionPerSecond
	}
	return fmt.Sprintf("%s%.2f/s%s", p.Prefix, positionPerSecond, p.Postfix)
}

func (p *ProgressPerTime) AddColor(color Color) *ProgressPerTime {
	p.Base.SetColor(color)
	return p
}
