package spb

import (
	"fmt"
	"time"
)

type Time struct {
	Base
	Value   time.Time
	SecStr  string
	MinStr  string
	HourStr string
}

func NewTime(base Base, time time.Time, secStr string, minStr string, hourStr string) *Time {
	return &Time{
		Base:    base,
		Value:   time,
		SecStr:  secStr,
		MinStr:  minStr,
		HourStr: hourStr,
	}
}

func NewTimeDefault(base Base) *Time {
	return NewTime(base, time.Now(), "s", "m", "h")
}

func (t *Time) Compile(pb *ProgressBar) string {
	return fmt.Sprintf("%s", ConvSec(*t, t.Value.Second()))
}

func (t Time) AddColor(color Color) Time {
	t.Base.SetColor(color)
	return t
}

func ConvSec(time Time, sec int) string {
	hours := sec / 3600
	minutes := (sec - (3600 * hours)) / 60
	sec = sec - (3600 * hours) - (minutes * 60)

	if hours < 1 {
		if minutes < 1 {
			return fmt.Sprintf("%d%s", sec, time.SecStr)
		}
		return fmt.Sprintf("%d%s%d%s", minutes, time.MinStr, sec, time.SecStr)
	}
	return fmt.Sprintf("%d%s%d%s", hours, time.HourStr, minutes, time.MinStr)
}
