package spb

import (
	"bufio"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

type ProgressBar struct {
	Current     uint // current Current
	Limit       uint // Limit value
	MaxDisabled bool
	Render      Render // renders function
	Mutex       sync.Mutex
}

// blocking
func NewCopy(from io.Reader, to io.Writer, options ...Options) error {
	defaultOptions := []Options{RenderFileNoSize}
	defaultOptions = append(defaultOptions, options...)
	pb := NewNoLimit(defaultOptions...)
	_, err := io.Copy(&Writer{
		Writer:      to,
		ProgressBar: pb,
	}, from)
	pb.Finish()
	if err != nil {
		return err
	}
	return nil
}

// blocking
func NewDownload(url string, to io.Writer, options ...Options) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status: %s", resp.Status)
	}
	defaultOptions := []Options{RenderFile(uint(resp.ContentLength), url)}
	defaultOptions = append(defaultOptions, options...)
	pb := New(uint(resp.ContentLength), defaultOptions...)
	_, err = io.Copy(&Writer{
		Writer:      to,
		ProgressBar: pb,
	}, resp.Body)
	pb.Finish()
	if err != nil {
		return err
	}
	return nil
}

// blocking
func NewByCall(channel chan uint, f func(chan uint), options ...Options) {
	go f(channel)
	value := <-channel
	pb := New(value, options...)
	for !pb.IsFinished() {
		value = <-channel
		pb.Advance(value)
	}
	pb.Finish()
}

func NewByFile(path string, options ...Options) (*ProgressBar, uint, error) {
	info, err := os.Stat(path)
	if os.IsNotExist(err) {
		return nil, 0, err
	}
	size := uint(info.Size())
	options = append(options, RenderFile(size, info.Name()))
	return New(size, options...), size, nil
}

func NewByPipe(options ...Options) (*ProgressBar, uint, error) {
	info, err := os.Stdin.Stat()
	if err != nil {
		return nil, 0, err
	}

	if info.Mode()&os.ModeNamedPipe == 0 {
		return nil, 0, fmt.Errorf("no Pipe found")
	}

	reader := bufio.NewReader(os.Stdin)
	var output []rune

	for {
		input, _, err := reader.ReadRune()
		if err != nil && err == io.EOF {
			break
		}
		output = append(output, input)
	}

	str := string(output)
	str = strings.TrimSpace(str)

	count, err := strconv.Atoi(str)

	if err != nil {
		return nil, 0, err
	}

	return New(uint(count), options...), uint(count), nil
}

func NewNoLimit(options ...Options) *ProgressBar {
	pb := ProgressBar{
		Current:     0,
		Limit:       1,
		Render:      &SimpleRender{},
		MaxDisabled: true,
	}
	OutputCLI(&pb) // default outputs
	RenderNoLimit(&pb)
	pb.SetOptions(options...)
	return &pb
}

func New(count uint, options ...Options) *ProgressBar {
	pb := ProgressBar{
		Current:     0,
		Limit:       count,
		Render:      &SimpleRender{},
		MaxDisabled: false,
	}

	OutputCLI(&pb)     // default outputs
	RenderDefault(&pb) // default renders
	pb.SetOptions(options...)
	return &pb
}

func (pb *ProgressBar) IsFinished() bool {
	if pb.MaxDisabled {
		return false
	}
	return pb.Current >= pb.Limit
}

func (pb *ProgressBar) Increment() *ProgressBar {
	pb.Advance(1)
	return pb
}

func (pb *ProgressBar) Decrement() *ProgressBar {
	pb.Reduce(1)
	return pb
}

func (pb *ProgressBar) Reduce(step uint) *ProgressBar {
	pb.Mutex.Lock()
	defer pb.Mutex.Unlock()
	if step > pb.Current {
		pb.Current = 0
	} else {
		pb.Current -= step
	}
	pb.Render.Draw(pb)
	return pb
}

func (pb *ProgressBar) Advance(step uint) *ProgressBar {
	pb.Mutex.Lock()
	defer pb.Mutex.Unlock()
	if pb.IsFinished() {
		return pb
	}
	if !pb.MaxDisabled {
		pb.Current += step
		if pb.Current > pb.Limit {
			pb.Current = pb.Limit
		}
	} else {
		pb.Current += step
	}
	pb.Render.Draw(pb)
	return pb
}

func (pb *ProgressBar) Set(value uint) *ProgressBar {
	pb.Mutex.Lock()
	defer pb.Mutex.Unlock()
	if !pb.MaxDisabled {
		if value > pb.Limit {
			pb.Current = pb.Limit
		} else {
			pb.Current = value
		}
	} else {
		pb.Current = value
	}
	pb.Render.Draw(pb)
	return pb
}

func (pb *ProgressBar) SelfRun(ms uint, offset uint) *ProgressBar {
	for !pb.IsFinished() {
		pb.Advance(offset)
		time.Sleep(time.Duration(ms) * time.Millisecond)
	}
	return pb
}

func (pb *ProgressBar) Clear() *ProgressBar {
	pb.Render.Clear(true)
	return pb
}

func (pb *ProgressBar) SetOptions(options ...Options) *ProgressBar {
	for _, option := range options {
		option(pb)
	}
	return pb
}

func (pb *ProgressBar) SetRender(render *SimpleRender) *ProgressBar {
	pb.Render = render
	return pb
}

func (pb *ProgressBar) SetOutput(output Output) *ProgressBar {
	pb.Render.SetOutput(output)
	return pb
}

func (pb *ProgressBar) Finish() {
	pb.Render.Finish()
}
