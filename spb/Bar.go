package spb

import (
	"fmt"
)

// Progress bar
type Bar struct {
	Base
	Fill  rune // Fill rune
	Empty rune // Empty rune
	Tip   rune // Tip rune
}

func NewBar(base Base, fill rune, empty rune, tip rune, static bool) *Bar {
	bar := Bar{
		Base:  base,
		Fill:  fill,
		Empty: empty,
		Tip:   tip,
	}
	bar.dynamic = !static
	return &bar
}

func (b *Bar) SetBarSize(size int) *Bar {
	b.Base.size = size
	return b
}

func (b *Bar) Compile(pb *ProgressBar) string {
	barSize := b.size - len(b.Prefix) - len(b.Postfix)
	percent := float64(pb.Current) / float64(pb.Limit)
	countFill := int(float64(barSize) * percent)
	content := ""
	for countFill > 1 {
		content += string(b.Fill)
		barSize--
		countFill--
	}
	if barSize > 0 {
		content += string(b.Tip)
		barSize--
	}
	for ; barSize > 0; barSize-- {
		content += string(b.Empty)
	}
	content = fmt.Sprintf("%s%s%s", b.Prefix, content, b.Postfix)
	return content
}

func (b *Bar) AddColor(color Color) *Bar {
	b.Base.SetColor(color)
	return b
}
