package spb

import "fmt"

// AbsoluteSize file value
type AbsoluteSize struct {
	Absolute
	Unit string
}

func NewAbsoluteSize(absolute Absolute, unit string) *AbsoluteSize {
	return &AbsoluteSize{
		Absolute: absolute,
		Unit:     unit,
	}
}

func (a *AbsoluteSize) Compile(pb *ProgressBar) string {
	position := ""
	if a.Unit == "" {
		unit := ConvUnitSize(float64(pb.Current))
		position = ConvFileSize(unit, float64(pb.Current))
	} else {
		position = ConvFileSize(a.Unit, float64(pb.Current))

	}
	content := fmt.Sprintf("%s%s%s", a.Prefix, position, a.Postfix)
	return content
}

func (a *AbsoluteSize) AddColor(color Color) *AbsoluteSize {
	a.Base.SetColor(color)
	return a
}

func ConvFileSize(unit string, value float64) string {
	switch unit {
	case "B":
		return fmt.Sprintf("%d %s", int(value), unit)
	case "kB":
		return fmt.Sprintf("%.1f %s", value/1000.0, unit)
	case "MB":
		return fmt.Sprintf("%.1f %s", value/1000000.0, unit)
	case "GB":
		return fmt.Sprintf("%.1f %s", value/1000000000.0, unit)
	}
	return fmt.Sprintf("%.1f %s", value, unit)
}

func ConvUnitSize(size float64) string {
	switch {
	case size < 1000.0:
		return "B"
	case size < 1000000.0:
		return "kB"
	case size < 1000000000.0:
		return "MB"
	case size < 1000000000000.0:
		return "GB"
	}
	return ""
}
