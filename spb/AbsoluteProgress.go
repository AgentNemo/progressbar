package spb

import (
	"fmt"
)

// AbsoluteProgress value
type AbsoluteProgress struct {
	Base
	Separator string // Separator
}

func NewAbsoluteProgress(base Base, separator string) *AbsoluteProgress {
	return &AbsoluteProgress{
		Base:      base,
		Separator: separator,
	}
}

func (a *AbsoluteProgress) Compile(pb *ProgressBar) string {
	content := fmt.Sprintf("%s%d%s%d%s", a.Prefix, pb.Current, a.Separator, pb.Limit, a.Postfix)
	return content
}

func (a *AbsoluteProgress) AddColor(color Color) *AbsoluteProgress {
	a.Base.SetColor(color)
	return a
}
