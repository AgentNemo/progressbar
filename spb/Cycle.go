package spb

import (
	"fmt"
	"time"
)

// Cycle
type Cycle struct {
	Base
	Cycles []Element
	index  int
	length int
	time   time.Time
	// Change configurations
	absoluteDuration   uint
	procentualDuration uint
	timeDuration       time.Duration
	everyChange        bool
}

func NewCycle(elements ...Element) *Cycle {
	return &Cycle{
		Cycles:             elements,
		index:              0,
		length:             len(elements),
		time:               time.Now(),
		absoluteDuration:   0,
		procentualDuration: 0,
		timeDuration:       0,
		everyChange:        false,
	}
}

func (c *Cycle) ProcentualDuration(duration uint) *Cycle {
	c.procentualDuration = duration
	return c
}

func (c *Cycle) AbsoluteDuration(duration uint) *Cycle {
	c.absoluteDuration = duration
	return c
}

func (c *Cycle) TimeDuration(duration time.Duration) *Cycle {
	c.timeDuration = duration
	return c
}

func (c *Cycle) EveryChange(value bool) *Cycle {
	c.everyChange = value
	return c
}

func (c *Cycle) Next(pb *ProgressBar) {
	if c.absoluteDuration != 0 {
		c.applyElement(int(pb.Current/c.absoluteDuration) % c.length)
	} else if c.procentualDuration != 0 {
		threshold := float64(pb.Limit) * (float64(c.procentualDuration) / 100.0)
		c.applyElement(int(float64(pb.Current)/threshold) % c.length)
	} else if c.timeDuration != 0 {
		now := time.Now()
		if now.Sub(c.time) > c.timeDuration {
			c.applyElement((c.index + 1) % c.length)
			c.time = now
		}
	} else if c.everyChange {
		c.applyElement((c.index + 1) % c.length)
	}
}

func (c *Cycle) applyElement(index int) {
	c.index = index
}

func (c *Cycle) Compile(pb *ProgressBar) string {
	if c.length == 0 {
		return ""
	}
	c.Next(pb)
	str := fmt.Sprintf("%s", c.Cycles[c.index].Compile(pb))
	return str
}

func (c *Cycle) Color() Color {
	return c.Cycles[c.index].Color()
}

func (c *Cycle) Dynamic() bool {
	return c.Cycles[c.index].Dynamic()
}

func (c *Cycle) SetSize(size int) {
	c.Cycles[c.index].SetSize(size)
}
