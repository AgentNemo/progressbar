package spb

import (
	"fmt"
)

// Static value
type Static struct {
	Base
	Value string
}

func NewStatic(base Base, value string) *Static {
	return &Static{
		Base:  base,
		Value: value,
	}
}

func (s *Static) Compile(pb *ProgressBar) string {
	return fmt.Sprintf("%s%s%s", s.Prefix, s.Value, s.Postfix)
}

func (s *Static) AddColor(color Color) *Static {
	s.Base.SetColor(color)
	return s
}
