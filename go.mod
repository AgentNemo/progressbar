module gitlab.com/AgentNemo/progressbar

go 1.16

require (
	github.com/olekukonko/ts v0.0.0-20171002115256-78ecb04241c0
	golang.org/x/sys v0.0.0-20210426230700-d19ff857e887 // indirect
)
